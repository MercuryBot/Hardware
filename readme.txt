This git contains electronic schematics for these parts of the project:
 - The receptor (+ FTDI chip shield)
 - The laser
 - The docking station
 - The motor drive

It also contains datasheets for different parts, and various footprints for these parts.